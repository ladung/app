package app

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync/atomic"
	"time"
)

type AppContext struct {
	Log           *zap.Logger `json:"-"`
	Db            *sql.DB     `json:"-"`
	Router        *mux.Router `json:"-"`
	CurrentUserId int64       `json:"-"`
}

type AppHandler struct {
	*AppContext
	H func(*AppContext, http.ResponseWriter, *http.Request)
}

type AuthlessHandler struct {
	*AppContext
	H func(*AppContext, http.ResponseWriter, *http.Request)
}

type httpResponseWriter struct {
	http.ResponseWriter
	status int
	length int
}

type Logger interface {
	logRequest(func(*AppContext, http.ResponseWriter, *http.Request)) func(*httpResponseWriter, *http.Request)
}

func (ctx *AppContext) Initialize() {
	var (
		logger *zap.Logger
		err    error
	)

	if os.Getenv(`APP_ENV`) == `production` {
		logger, err = zap.NewProduction()
	} else {
		logger, err = zap.NewDevelopment()
	}

	if err != nil {
		log.Fatalf("Can't initialize zap logger: %v", err)
	}
	//defer logger.Sync()

	dbConnSpec := fmt.Sprintf("postgres://%s:%s@%s/%s?sslmode=disable",
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_DB"))

	db, err := sql.Open("postgres", dbConnSpec)
	if err != nil {
		logger.Fatal("opening db connectoin failed", zap.Error(err))
	}

	//defer db.Close()

	err = db.Ping()
	if err != nil {
		logger.Fatal("database is not available", zap.Error(err))
	}

	logger.Info("Server is starting")

	ctx.Log = logger
	ctx.Db = db
	ctx.Router = mux.NewRouter()
}

func (w *httpResponseWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}

func (w *httpResponseWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (ctx *AppContext) logRequest(next func(*AppContext, http.ResponseWriter, *http.Request)) func(*httpResponseWriter, *http.Request) {
	return func(w *httpResponseWriter, r *http.Request) {
		start := time.Now()
		log := ctx.Log
		body := ``

		if os.Getenv(`APP_ENV`) != `production` && r.Method != "GET" && r.Body != nil {
			bytesBody, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Error(`Failed create buffer`, zap.Error(err))
				return
			}
			body = string(bytesBody[:])
			r.Body = ioutil.NopCloser(bytes.NewBuffer(bytesBody))
		}

		next(ctx, w, r)

		duration := time.Now().Sub(start)

		log.Info(r.Method+" "+r.URL.Path,
			zap.String(`method`, r.Method),
			zap.String(`clientip`, IpAddr(r)),
			zap.String(`user_agent`, r.UserAgent()),
			zap.String(`request_body`, body),
			zap.Int(`status`, w.status),
			zap.Int(`content_len`, w.length),
			zap.Int64(`duration`, int64(duration)),
		)
	}
}

func (h AppHandler) authRequest(next func(*AppContext, http.ResponseWriter, *http.Request)) func(*AppContext, http.ResponseWriter, *http.Request) {
	return func(ctx *AppContext, w http.ResponseWriter, r *http.Request) {
		log := ctx.Log
		signKey := os.Getenv(`LADUNG_SECRET_KEY`)
		authToken := r.Header.Get(`X-Authorization`)

		if authToken != "" {
			token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, errors.New("Error during parse jwt")
				}
				return []byte(signKey), nil
			})

			if err != nil {
				log.Debug("Error: ", zap.Error(err))
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			if token.Valid {
				var userId int64

				sqlCheck := `SELECT id FROM devices WHERE jwt_token = $1 LIMIT 1`
				err := ctx.Db.QueryRow(sqlCheck, authToken).Scan(&userId)
				if err == sql.ErrNoRows {
					log.Debug("Error: ", zap.Error(err))
					w.WriteHeader(http.StatusUnauthorized)
					return
				} else if err != nil {
					log.Error("Error: ", zap.Error(err))
					w.WriteHeader(http.StatusInternalServerError)
					return
				}

				ctx.CurrentUserId = userId

				next(ctx, w, r)
			} else {
				log.Debug("Error: invalid jwt")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		} else {
			log.Debug("Error: empty header")
			w.WriteHeader(http.StatusUnauthorized)
		}
	}
}

func (h AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sw := httpResponseWriter{ResponseWriter: w}

	h.logRequest(h.authRequest(h.H))(&sw, r)
}

func (h AuthlessHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sw := httpResponseWriter{ResponseWriter: w}
	h.logRequest(h.H)(&sw, r)
}

type HealthCheckHandler struct {
	Healthy int32
}

func (h HealthCheckHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if atomic.LoadInt32(&h.Healthy) == 1 {
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-Content-Type", "application/json; charset=utf-8")
		io.WriteString(w, `{"alive": true}`)

		return
	}
	w.WriteHeader(http.StatusServiceUnavailable)
}

type FwdToZapWriter struct {
	Logger *zap.Logger
}

func (fw *FwdToZapWriter) Write(p []byte) (n int, err error) {
	fw.Logger.Error(string(p))
	return len(p), nil
}
