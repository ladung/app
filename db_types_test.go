package app

import (
	"testing"
)

func TestMarshalJSON(t *testing.T) {
	t.Run("When valid", func(t *testing.T) {
		x := NullInt64{Valid: true, Int64: int64(10)}

		marshaled, err := x.MarshalJSON()
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		val := string(marshaled[:])

		if val != "10" {
			t.Errorf("MarshalJSON() = %s; wanted 10", val)
		}
	})

	t.Run("When invalid", func(t *testing.T) {
		x := NullInt64{Valid: false, Int64: int64(10)}

		marshaled, err := x.MarshalJSON()
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		val := string(marshaled[:])

		if val != "null" {
			t.Errorf("MarshalJSON() = %s; wanted null", val)
		}
	})
}

func TestUnmarshalJSON(t *testing.T) {
	t.Run("When valid", func(t *testing.T) {
		x := NullInt64{}
		s := `10`

		err := x.UnmarshalJSON([]byte(s))
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		if x.Int64 != 10 {
			t.Errorf("UnmarshalJSON(10) = %d; wanted 10", x.Int64)
		}
	})

	t.Run("When invalid", func(t *testing.T) {
		x := NullInt64{}
		s := `null`

		err := x.UnmarshalJSON([]byte(s))
		if err != nil {
			t.Errorf("Error: %v", err)
		}

		if x.Int64 != 0 {
			t.Errorf("Int64 = %d; wanted 0", x.Int64)
		}

		if x.Valid != false {
			t.Errorf("Valid = %v; wanted false", x.Valid)
		}
	})
}
